import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LookupMaintenanceComponent } from './lookup-maintenance/lookup-maintenance.component';
import { LookupInventoryComponent } from './lookup-inventory/lookup-inventory.component';
import { DataService } from './data.service';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { CommercialKitComponent } from './commercial-kit/commercial-kit.component';
import { HttpModule }    from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AuthHandler } from  './app-settings/auth-util/auth-handler';
import { PermissionsHandler } from './app-settings/auth-util/permissions-handler'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AboutComponent,
    HeaderComponent,
    FooterComponent,
    LookupMaintenanceComponent,
     LookupInventoryComponent,
     CommercialKitComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    JwtModule,
    FormsModule,
    HttpModule
  ],
  providers: [DataService,CookieService,AuthGuardService,AuthService,JwtHelperService,AuthHandler,PermissionsHandler],
  bootstrap: [AppComponent]
})
export class AppModule { }
